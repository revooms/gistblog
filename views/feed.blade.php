<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">

  <title>{{ $gb->title }}</title>
  <subtitle>Ein gistblog von {{ $gb->author }}. https://gist.github.com/{{ $gb->id }}</subtitle>
  <link href="https://gist.github.com/{{ $gb->id }}" rel="self" />
  <link href="https://gist.github.com/{{ $gb->id }}" />
  <id>{{ $gb->url }}/blog/{{ $gb->id }}</id>
  <updated>{{ $gb->updated }}</updated>

  @foreach($gb->comments as $comment)
  <entry>
    <title>{{ $comment['title'] }}</title>
    <link href="{{ $gb->url }}/#p{{ $comment['id'] }}" />
    <id>{{ $gb->url }}/#p{{ $comment['id'] }}</id>
    <updated>{{ $comment['published'] }}</updated>
    <summary type="xhtml">
      <div xmlns="http://www.w3.org/1999/xhtml">
        {!! $comment['body'] !!}
      </div>
    </summary>
    <author>
      <name>{{ $gb->author }}</name>
    </author>
  </entry>
  @endforeach
</feed>
