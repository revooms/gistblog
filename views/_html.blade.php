<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{ $gb->title }} von {{ $gb->author }} :: gistblog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="referrer" content="origin" />
    <meta name="author" content="{{ $gb->author }}">
    @if(isset($gb->description))
    <meta name="description" content="{{ $description }}">
    @endif
    @if(isset($gb->keywords))
    <meta name="keywords" content="gistblog,gist,blog">
    @endif
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/flexboxgrid/6.3.1/flexboxgrid.min.css" type="text/css" >
    <link href="feed.php" type="application/atom+xml" rel="alternate" title="{{ $gb->title }} atom feed" />
    <style>
    @font-face {
        font-family: 'Miriam-Libre-Regular';
        src:url('fonts/Miriam-Libre-Regular.ttf.woff') format('woff'),
        url('fonts/Miriam-Libre-Regular.ttf.svg#Miriam-Libre-Regular') format('svg'),
        url('fonts/Miriam-Libre-Regular.ttf.eot'),
        url('fonts/Miriam-Libre-Regular.ttf.eot?#iefix') format('embedded-opentype');
        font-weight: normal;
        font-style: normal;}

        body {
            font-family: 'Miriam-Libre-Regular';
        }

        blockquote {
            border-left: 5px solid #ccc;
            padding-left: 17px;
            color: #666;
        }

        code {
            background-color: #eee;
            padding: 4px;
        }

        h1,h2,h3,h4,h5,h6 {
            border-bottom: 1px solid #def;
        }

        h1 > small, h2 > small, h3 > small {
            color: #666;
        }

        footer {
            margin-top: 32px;
        }

        .container {
            max-width: 600px ;
            padding: 6px;
        }
        @media only screen and (min-width: 480px) {
            .container {
                padding: 20px;
            }
        }

        .post {
            margin-top: 96px;
        }
        .post img {
            max-width:100%;
            margin:auto;
        }
    </style>
</head>
<body class="gistblog">

    <div class="container">
        <div class="row">
            <header class="">
                <nav class="main">
                    <h1>{{ $gb->title }} <small>Von {{ $gb->author }}</small></h1>
                </nav>
            </header>
        </div>

        <div class="row">
            <main>
                @yield('content')
            </main>
        </div>

        <div class="row">
            <footer>
                <p>
                    Powered by gistblog
                    <small>
                        <a href="https://gist.github.com/{{ $gb->id }}">{{ $gb->id }}</a>
                    </small>
                </p>
                <p>
                    <a href="feed.php">feed</a>
                </p>
            </footer>
        </div>
    </div>

</body>
</html>
