@extends('_html')

@section('content')
<div class="posts">

    @foreach($gb->comments as $comment)

    <div class="post" id="p{{ $comment['id'] }}">
        <div class="published">
            {!! $comment['published'] !!}
        </div>
        {!! $comment['body'] !!}
    </div>

    @endforeach

</div>
@endsection
