<?php

use Jenssegers\Blade\Blade;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Kevinrob\GuzzleCache\CacheMiddleware;
use Kevinrob\GuzzleCache\Storage\Psr6CacheStorage;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

require __DIR__ . '/vendor/autoload.php';

function dd($object)
{
    return sprintf("<p><pre>%s</pre></p>", var_export($object));
}

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

$blade = new Blade('views', 'cache/blade');

if (getenv('DEBUG') == true) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

class Gistblog
{
    public $author;
    public $since;
    public $title;
    public $updated;
    public $url;

    public $comments = [];

    private $client;
    private $gistId;

    public function __construct()
    {
        //GUZZLE
        $stack = HandlerStack::create(); // Create a HandlerStack
        $cache_strategy_class = '\Kevinrob\GuzzleCache\Strategy\PrivateCacheStrategy'; // Choose a cache strategy: the PrivateCacheStrategy is good to start with
        $cache_storage = new Psr6CacheStorage(new FilesystemAdapter('', 0, 'cache/guzzle'), getenv('CACHESECONDS')); // Instantiate the cache storage: a PSR-6 file system cache
        $stack->push(new CacheMiddleware(new $cache_strategy_class($cache_storage)), 'cache'); // Add cache middleware to the top of the stack with `push`

        // Initialize the client with the handler option
        $this->client = new Client([
            'handler' => $stack,
            'base_uri' => 'https://api.github.com/gists/',
            'timeout'  => 2.0
        ]);

        $this->url = getenv('URL');
        $this->gistId = getenv('GIST_ID');
        $comments = $this->getComments();
        $info = $this->getGistInfo();
    }

    public function getGistInfo()
    {
        $url = 'https://api.github.com/gists/' . $this->gistId;
        $json = $this->http($url);
        $response = json_decode($json);
        $this->id = $this->gistId;
        $this->title = $response->description;
        $this->author = $response->owner->login;
        $this->since = $response->created_at;
        $this->updated = $this->comments[0]['published'];
    }

    public function getComments()
    {
        $url = 'https://api.github.com/gists/' . $this->gistId . '/comments';
        $json = $this->http($url);
        $response = json_decode($json);
        arsort($response);
        foreach ($response as $comment) {
            if ($comment->author_association == "OWNER") {
                $c['id'] = $comment->id;
                $c['title'] = strtok($comment->body, "\n");
                $c['body'] = $this->md($comment->body);
                $c['published'] = $comment->created_at;
                array_push($this->comments, $c);
            }
        }
    }

    public function http($requestUrl)
    {
        $query = http_build_query([
            'client_id' => getenv('CLIENTID'),
            'client_secret' => getenv('CLIENTSECRET')
        ]);

        $response = $this->client->get($requestUrl, [
            'query' => $query
        ]);

        if ($response->getStatusCode() == 200) {
            return $response->getBody();
        }
    }
    public function md($markdown)
    {
        $parsed = Parsedown::instance()
        ->setBreaksEnabled(true)
        ->text($markdown);

        return $parsed;
    }
}
